var Worker = (function() {

	var worker  = {};

	// demoList
	var demoList = [
		'bat',
		'cat',
		'bread',
		'foo',
		'bar',
		'dough',
		'plane',
		'ring',
		'train',
		'brain',
		'node',
		'batter',
		'ball',
		'car',
		'bag',
		'beg',
		'ceg',
		'bed'
	];

	// digitMmap
	var digitMap = {
		'2': 'abc',
		'3': 'def',
		'4': 'ghi',
		'5': 'jkl',
		'6': 'mno',
		'7': 'pqrs',
		'8': 'tuv',
		'9': 'wxyz'
	};

	// return array from demoList that contain the
	// same number of characters
	function filterDemoList(numOfChars) {
		var list = [];
		for (var i in demoList) {
			if (demoList[i].length == numOfChars) {
				list.push(demoList[i]);
			}
		}
		return list;
	}

	function decodeInput(stringOfNumbers) {
		var digitArr = [];
		for (var i in stringOfNumbers.split("")) {
			digitArr.push(digitMap[stringOfNumbers[i]].trim());
		}
		return digitArr;
	}

	function queryDemoList(demoList,digitMap) {
		var list = [];
		for (var i in demoList) { // "bat"
			var wordLength = demoList[i].length;
			outer:
			for (var j in demoList[i].split("")) { // "bat" => 'b,a,t'
				var dChar = demoList[i].split("")[j];
				var re = new RegExp('[' + digitMap[j] + ']');
				var pos = dChar.search(re);
				if (pos == -1 ) {
					demoList.splice(i, 1);
					break outer;
				//	console.log('new demolist ' + demoList);
				} else if ( pos != -1 && (wordLength-1) == j) {
					list.push(demoList[i]);
				}
			}
		}
		//console.log(list);
		 return list;
	}

	function queryDigitMap(stringOfNumbers) {
		var filteredDemoList = filterDemoList(stringOfNumbers.length); // i.e., ['bat',cat']
		var digitMapArr = decodeInput(stringOfNumbers); //i.e., ['jkl','tuv','abc']
    return queryDemoList(filteredDemoList,digitMapArr)
	}


	worker.getWords = function(stringOfNumbers,cb) {
		if (stringOfNumbers < 1) {
			var e = new Error('Input not a string');
			cb(e, null);
			return;
		} else {
			cb(null,queryDigitMap(stringOfNumbers));
		}
	};

	return worker;
});

module.exports = Worker;

